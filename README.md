# bike-camera-action

**Purpose**
Build an app that allows the user to upload video files from bike cameras (e.g. mp4 format) and have AI pull out relevant information.  
e.g.
- if the rider has said the word "keep" at any time on the ride, the AI should detect that and retrieve the slice of video file for that point
(or 10s before and after)
- the app should also retrieve the matching rear-camera footage for the same time slice
- the user should not have to do anything more than upload the files

## Tech
For completeness, the app should have all code necessary to work on the 3 main clouds:
- Azure
- GCP
- AWS

Code for each should be kept in separate folders.

